import React from "react";
import { HealthFigure } from "../graphs/HealthFigure.js";
import "../css/Hfigure.css";
import { Header, Card } from "semantic-ui-react";

class Hfigure extends React.PureComponent {
  componentDidMount() {
    this.draw();
  }

  componentDidUpdate() {
    this.removePreviousChart();
    this.draw();
  }
  removePreviousChart() {
    const chart = document.getElementById("hGraph");
    while (chart.hasChildNodes()) chart.removeChild(chart.lastChild);
  }
  draw() {
    var that = this;
    var className = "hGraph";
    var width = this.props.width - 150;
    var hFigureInstance = HealthFigure(
      this.props.title,
      this.props.data,
      width,
      className,
      {
        minScale: 1
      },
      that.props.selectFactor
    ); // from data.js
    //var additionalFigure = hFigureInstance.plotAt(0); // from data.js
  }
  render() {
    return (
      <Card color="blue" fluid>
        <Card.Content>
          <Card.Header as="h3">Health Graph</Card.Header>
        </Card.Content>
        <Card.Content>
          <div id="hGraph" />
        </Card.Content>
        <Card.Content>
          <Card.Description>
            <Header>A closer look at your health</Header>The health graph shows
            a holistic view of your health status in terms of your modifiable
            risk factors. These risk factors are called modifiable because you
            can control them to reduce your health risks, and in turn avoid
            chronic diseases. A quick look at your Health Graph tells you which
            risk factors are affecting your health and risk of chronic diseases.
            The green area indicates the ‘recommended’ healthy range. Any risk
            factor falling below or above the purple area appears as either red
            (very high risk) or yellow (high risk).
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}
Hfigure.defaultProps = {
  data: [],
  title: "Overall Risk Factors",
  width: 600
};
export default Hfigure;
