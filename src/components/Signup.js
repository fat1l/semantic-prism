import React, { Component } from "react";
import {
	Button,
	Checkbox,
	Form,
	Input,
	Radio,
	Select,
	TextArea,
	Container,
	Step
} from "semantic-ui-react";

const options = [
	{ key: "m", text: "Male", value: "male" },
	{ key: "f", text: "Female", value: "female" }
];

class Signup extends Component {
	state = {};

	handleChange = (e, { name, value }) =>
		this.setState({
			[name]: {
				citizenID: 3,
				date_taken: Date.now(),
				elementId: name,
				value: value
			}
		});
	render() {
		const { A1, A2, A3, A4, A5 } = this.state;
		return (
			<Container>
				<Step.Group fluid>
					<Step active>
						<Step.Content>
							<Step.Title>Section A</Step.Title>
						</Step.Content>
					</Step>

					<Step inactive>
						<Step.Content>
							<Step.Title>Section B</Step.Title>
						</Step.Content>
					</Step>

					<Step inactive>
						<Step.Content>
							<Step.Title>Section C</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section D</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section E</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section F</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section G</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section H</Step.Title>
						</Step.Content>
					</Step>
					<Step inactive>
						<Step.Content>
							<Step.Title>Section I</Step.Title>
						</Step.Content>
					</Step>
				</Step.Group>
				<Form>
					<Form.Group widths="equal">
						<Form.Field
							control={Input}
							name="fname"
							label="First name"
							placeholder="First name"
						/>
						<Form.Field
							control={Input}
							name="lname"
							label="Last name"
							placeholder="Last name"
						/>
						<Form.Field
							control={Select}
							name="A1"
							label="What sex were you assigned at birth"
							placeholder="Gender at Birth"
							options={options}
							onChange={this.handleChange}
						/>
					</Form.Group>
					<Form.Group>
						<label>Which best describes your current gender identity?</label>
					</Form.Group>
					<Form.Group inline>
						<Form.Field
							control={Radio}
							name="A2"
							label="Male"
							value="male"
							checked={A2 === "male"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A2"
							label="Female"
							value="female"
							checked={A2 === "female"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A2"
							label="Indigenous or other cultural minority identity (such as two-spirit)"
							value="ig"
							checked={A2 === "ig"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A2"
							label="Other (such as gender fluid or non-binary)"
							value="fluid"
							checked={A2 === "fluid"}
							onChange={this.handleChange}
						/>
					</Form.Group>
					{(A1 === "male") &
					(A2 !== "female") &
					((A2 === "male") | (A2 === "fluid") | (A2 === "ig")) ? (
						<Form.Group inline>
							<label>
								What gender do you currently live in your day-to-day life
							</label>
							<Form.Field
								control={Radio}
								name="A3"
								label="Male"
								value="male"
								checked={A3 === "male"}
								onChange={this.handleChange}
							/>
							<Form.Field
								control={Radio}
								name="A3"
								label="Female"
								value="female"
								checked={A3 === "female"}
								onChange={this.handleChange}
							/>
							<Form.Field
								control={Radio}
								name="A3"
								label="Sometime male, sometimes female"
								value="Sometime male, sometimes female"
								checked={A3 === "Sometime male, sometimes female"}
								onChange={this.handleChange}
							/>
							<Form.Field
								control={Radio}
								name="A3"
								label="Other than male, female"
								value="Other than male, female"
								checked={A3 === "Other than male, female"}
								onChange={this.handleChange}
							/>
						</Form.Group>
					) : null}
					<Form.Field
						control={Input}
						type="date"
						name="A4"
						label="Enter your Date of Birth"
						onChange={this.handleChange}
					/>
					<Form.Group>
						<label>
							Which of the following groups best describe your cultural or
							ethnic background?
						</label>
					</Form.Group>
					<Form.Group>
						<Form.Field
							control={Radio}
							name="A5"
							label="African (Black)"
							value="African (Black)"
							checked={A5 === "African (Black)"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="Caucasian (White)"
							value="Caucasian (White)"
							checked={A5 === "Caucasian (White)"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="Afro-Caribbean"
							value="Afro-Caribbean"
							checked={A5 === "Afro-Caribbean"}
							onChange={this.handleChange}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Field
							control={Radio}
							name="A5"
							label="Latin American (Hispanic)"
							value="Latin American (Hispanic)"
							checked={A5 === "Latin American (Hispanic)"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="East Asian (Chinese, Vietnamese, Japanese, Korean)"
							value="East Asian (Chinese, Vietnamese, Japanese, Korean)"
							checked={
								A5 === "East Asian (Chinese, Vietnamese, Japanese, Korean)"
							}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="South Asian (Indian, Pakistani, Sri Lankan)"
							value="South Asian (Indian, Pakistani, Sri Lankan)"
							checked={A5 === "South Asian (Indian, Pakistani, Sri Lankan)"}
							onChange={this.handleChange}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Field
							control={Radio}
							name="A5"
							label="Arab"
							value="Arab"
							checked={A5 === "Arab"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="West Asian (Iranian, Afghan)"
							value="West Asian (Iranian, Afghan)"
							checked={A5 === "West Asian (Iranian, Afghan)"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="Indigenous (Canadian)"
							value="Indigenous (Canadian)"
							checked={A5 === "Indigenous (Canadian)"}
							onChange={this.handleChange}
						/>
						<Form.Field
							control={Radio}
							name="A5"
							label="Native American or Alaskan Native"
							value="Native American or Alaskan Native"
							checked={A5 === "Native American or Alaskan Native"}
							onChange={this.handleChange}
						/>
					</Form.Group>
					<Form.Field control={Button}>Next</Form.Field>
				</Form>
			</Container>
		);
	}
}
export default Signup;
