import React from "react";
import {
	Button,
	Form,
	Grid,
	Header,
	Message,
	Segment
} from "semantic-ui-react";

class Login extends React.Component {
	state = { email: "", password: "" };

	handleChange = (e, { name, value }) => this.setState({ [name]: value });

	handleSubmit = () => {
		const { email, password } = this.state;
		this.setState({ email: email, password: password });
		console.log(this.state);
		this.props.history.push(`/home`);
	};
	render() {
		const { email, password } = this.state;
		return (
			<div className="login-form">
				{/*
		      Heads up! The styles below are necessary for the correct render of this example.
		      You can do same with CSS, the main idea is that all the elements up to the `Grid`
		      below must have a height of 100%.
		    */}
				<style>{`
					body {
						background: #2185D0; /* fallback for old browsers */
						background: -webkit-linear-gradient(right, #2185D0, #5994f2);
						background: -moz-linear-gradient(right, #2185D0, #5994f2);
						background: -o-linear-gradient(right, #2185D0, #5994f2);
						background: linear-gradient(to left, #2185D0, #5994f2);
						font-family: "Roboto", sans-serif;
						-webkit-font-smoothing: antialiased;
						-moz-osx-font-smoothing: grayscale;
					}
		      body > div,
		      body > div > div,
		      body > div > div > div.login-form {
		        height: 100%;
		      }
		    `}</style>
				<Grid
					textAlign="center"
					style={{ height: "100%" }}
					verticalAlign="middle"
				>
					<Grid.Column style={{ maxWidth: 450 }}>
						<Header as="h2" color="grey" textAlign="center">
							Log-in to your account
						</Header>
						<Form onSubmit={this.handleSubmit} size="large">
							<Segment stacked>
								<Form.Input
									fluid
									icon="user"
									iconPosition="left"
									placeholder="E-mail address"
									name="email"
									type="email"
									value={email}
									onChange={this.handleChange}
								/>
								<Form.Input
									fluid
									icon="lock"
									iconPosition="left"
									placeholder="Password"
									name="password"
									type="password"
									value={password}
									onChange={this.handleChange}
								/>

								<Button color="blue" fluid size="medium">
									Login
								</Button>
							</Segment>
						</Form>
						<Message>
							New to us? <a href="#">Sign Up</a>
						</Message>
					</Grid.Column>
				</Grid>
			</div>
		);
	}
}

export default Login;
