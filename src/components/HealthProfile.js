import React, { Component } from "react";
import {
  Button,
  Checkbox,
  Form,
  Input,
  Radio,
  Select,
  TextArea,
  Container
} from "semantic-ui-react";

const options = [
  { key: "m", text: "Male", value: "Male" },
  { key: "f", text: "Female", value: "Female" }
];

class HealthProfile extends Component {
  state = {};

  //handleChange = (e, { value }) => this.setState({ value });
  handleChange = (e, { name, value }) => this.setState({ [name]: value });
  render() {
    const { A1, A2, A3, A4, A5 } = this.state;
    return (
      <Container>
        <Form>
          <Form.Group widths="equal">
            <Form.Field
              control={Radio}
              name="fname"
              label="First name"
              placeholder="First name"
            />
            <Form.Field
              control={Input}
              name="lname"
              label="Last name"
              placeholder="Last name"
            />
          </Form.Group>
          <Form.Group inline>
            <label>
              What sex were you assigned at birth (on your original birth
              certificate)
            </label>
            <Form.Field
              control={Radio}
              name="A1"
              label="Male"
              value="male"
              checked={A1 === "male"}
              onChange={this.handleChange}
            />
            <Form.Field
              control={Radio}
              name="A1"
              label="Female"
              value="female"
              checked={A1 === "female"}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group inline>
            <label>Which best describes your current gender identity?</label>
            <Form.Field
              control={Radio}
              name="A2"
              label="Male"
              value="male"
              checked={A2 === "male"}
              onChange={this.handleChange}
            />
            <Form.Field
              control={Radio}
              name="A2"
              label="Female"
              value="female"
              checked={A2 === "female"}
              onChange={this.handleChange}
            />
            <Form.Field
              control={Radio}
              name="A2"
              label="Indigenous or other cultural minority identity (such as two-spirit)"
              value="ig"
              checked={A2 === "ig"}
              onChange={this.handleChange}
            />
            <Form.Field
              control={Radio}
              name="A2"
              label="Other (such as gender fluid or non-binary)"
              value="fluid"
              checked={A2 === "fluid"}
              onChange={this.handleChange}
            />
          </Form.Group>
          {(A1 === "male") & (A2 !== "female") ? (
            <Form.Group>
              <label>
                What gender do you currently live in your day-to-day life
              </label>
              <Form.Field
                control={Radio}
                name="A3"
                label="Male"
                value="male"
                checked={A3 === "male"}
                onChange={this.handleChange}
              />
              <Form.Field
                control={Radio}
                name="A3"
                label="Female"
                value="female"
                checked={A3 === "female"}
                onChange={this.handleChange}
              />
              <Form.Field
                control={Radio}
                name="A3"
                label="Sometime male, sometimes female"
                value="Sometime male, sometimes female"
                checked={A3 === "Sometime male, sometimes female"}
                onChange={this.handleChange}
              />
              <Form.Field
                control={Radio}
                name="A3"
                label="Other than male, female"
                value="Other than male, female"
                checked={A3 === "Other than male, female"}
                onChange={this.handleChange}
              />
            </Form.Group>
          ) : null}
          <Form.Field
            control={Input}
            type="date"
            name="A4"
            label="Enter your Date of Birth"
            onChange={this.handleChange}
          />
          <Form.Group>
            <label>
              Which of the following groups best describe your cultural or
              ethnic background?
            </label>
            <Form.Field
              control={Radio}
              name="A5"
              label="African (Black)"
              value="African (Black)"
              checked={A5 === "African (Black)"}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Field
            control={Checkbox}
            label="I agree to the Terms and Conditions"
          />
          <Form.Field control={Button}>Submit</Form.Field>
        </Form>
      </Container>
    );
  }
}
export default HealthProfile;
