import React from "react";
import { Statistic, Segment, Icon, Progress } from "semantic-ui-react";

class Metrics extends React.Component {
	render() {
		const has_color = (parseFloat(this.props.has) < 30? "green": parseFloat(this.props.has) < 80? "orange":"red")
		const risk_level = (parseFloat(this.props.has) === 0? "thermometer empty":parseFloat(this.props.has) < 30? "thermometer quarter": parseFloat(this.props.has) < 80? "thermometer three quarters":"thermometer full")
		return (
			<Segment>
				<Statistic.Group widths="four">
					<Statistic color={has_color}>
						<Statistic.Value>
							<Icon size="small" name={risk_level} />
							{parseFloat(this.props.has)}
						</Statistic.Value>
						<Statistic.Label>Health Asset Score</Statistic.Label>
					</Statistic>
					<Statistic color="red">
						<Statistic.Value>
							<Icon size="small" name="warning sign" />
							{this.props.risknumber}
						</Statistic.Value>
						<Statistic.Label>Diseases Assessed</Statistic.Label>
					</Statistic>
					<Statistic color="green">
						<Statistic.Value>
							<Icon size="small" name="bar chart" />
							{this.props.obs}
						</Statistic.Value>
						<Statistic.Label>Risk Factors Tracked</Statistic.Label>
					</Statistic>
					<Statistic color="blue">
						<Statistic.Value>
							<Icon size="small" name="time" />
							{this.props.days}
						</Statistic.Value>
						<Statistic.Label>Days Since Last Track</Statistic.Label>
					</Statistic>
				</Statistic.Group>
			</Segment>
		);
	}
}
Metrics.defaultProps = {
	risknumber: 0,
	obs: 0,

	// guideline Postions
	days: 0,
	has: 0.0
};
export default Metrics;
