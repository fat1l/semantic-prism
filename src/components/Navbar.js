import React, { Component } from "react";
import {
	Sidebar,
	Segment,
	Button,
	Menu,
	Image,
	Icon,
	Header
} from "semantic-ui-react";

class Navbar extends Component {
	state = { visible: false };

	toggleVisibility = () => this.setState({ visible: !this.state.visible });

	render() {
		const { visible } = this.state;
		return (
			<div>
				<Button onClick={this.toggleVisibility}>Toggle Visibility</Button>
				<Sidebar.Pushable as={Segment}>
					<Sidebar
						as={Menu}
						animation="uncover"
						width="thin"
						visible={visible}
						icon="labeled"
						vertical
						inverted
					>
						<Menu.Item name="home">
							<Icon name="home" />
							Home
						</Menu.Item>
						<Menu.Item name="gamepad">
							<Icon name="gamepad" />
							Games
						</Menu.Item>
						<Menu.Item name="camera">
							<Icon name="camera" />
							Channels
						</Menu.Item>
					</Sidebar>
					<Sidebar.Pusher>
						<Segment basic>
							<Header as="h3">Application Content</Header>
							jQuery is a DOM manipulation library. It reads from and writes to
							the DOM. React uses a virtual DOM (a JavaScript representation of
							the real DOM). React only writes patch updates to the DOM, but
							never reads from it. It is not feasible to keep real DOM
							manipulations in sync with React's virtual DOM. Because of this,
							all jQuery functionality has been re-implemented in React.
							Declarative API Declarative APIs provide for robust features and
							prop validation. Control the rendered HTML tag, or render one
							component as another component. Extra props are passed to the
							component you are rendering as. Augmentation is powerful. You can
							compose component features and props without adding extra nested
							components. This is essential for working with MenuLinks and
							react-router.Shorthand Props Shorthand props generate markup for
							you, making many use cases a breeze. All object props are spread
							on the child components. Child Object Arrays Components with
							repeating children accept arrays of plain objects. Facebook is
							fond of this over using context to handle parent-child coupling
							and so are we. JSX icon The icon prop is standard for many
							components. It can accept an Icon name, an Icon props object, or
							an <Icon /> instance.mage The image prop is standard for many
							components. It can accept an image src, an Image props object, or
							an <Image /> instance.
						</Segment>
					</Sidebar.Pusher>
				</Sidebar.Pushable>
			</div>
		);
	}
}

export default Navbar;
