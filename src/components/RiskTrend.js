import React from "react";
import d3 from "d3";
import { risk_line } from "../graphs/Riskline.js";
import { Card, Divider } from "semantic-ui-react";

class RiskTrend extends React.Component {
  componentDidMount() {
    //this.removePreviousChart();
    //this.draw();
  }
  componentDidUpdate() {
    this.removePreviousChart();
    this.draw();
  }
  removePreviousChart() {
    const chart = document.getElementById("RiskTrend");
    while (chart.hasChildNodes()) chart.removeChild(chart.lastChild);
  }
  draw() {
    var risk_chart = risk_line()
      .width(document.getElementById("RiskTrend").clientWidth)
      .height(this.props.height)

      // guideline Postions
      .top_guideline(this.props.top_guideline)
      .bottom_guideline(this.props.bottom_guideline)

      // Side legend variables
      .side_text_top(this.props.side_text_top)
      .side_text_mid(this.props.side_text_mid)
      .side_text_bot(this.props.side_text_bot)

      //gradient changes
      .gradient_id("areagradient")
      .red_max(this.props.red_max)
      .yellow_max(this.props.yellow_max)
      .min(this.props.min)
      //.yellow_max(this.props.yellow_max)

      //Title and Subtitle
      .title(this.props.data.label)
      .subtitle(this.props.subtitle)

      //ylabel unit
      .ylabel(this.props.ylabel);
    if (this.props.data.samples) {
      d3
        .select("#RiskTrend")
        .datum(this.props.data.samples)
        .call(risk_chart);
    }
  }
  render() {
    return (
      <Card color="blue" fluid>
        <Card.Content>
          <Card.Header as="h3">Risk Trend</Card.Header>
        </Card.Content>
        <Card.Content>
          <div id="RiskTrend" />
        </Card.Content>
      </Card>
    );
  }
}
RiskTrend.defaultProps = {
  width: 1000,
  height: 350,

  // guideline Postions
  top_guideline: 80,
  bottom_guideline: 30,

  // Side legend variables
  side_text_top: "High",
  side_text_mid: "Moderate",
  side_text_bot: "Low",

  //gradient changes
  red_max: 80,
  yellow_max: 30,
  min: 30,
  red_min: 0,
  //yellow_min: 10,
  //yellow_max: 50,

  //Title and Subtitle
  title: "",
  subtitle: "",

  //ylabel unit
  ylabel: ""
};
export default RiskTrend;
