import React, { Component } from "react";
import {
	Sidebar,
	Menu,
	Icon,
	Header,
	Segment,
	Responsive,
	Grid,
	Card,
	Divider,
	Popup,
	Container
} from "semantic-ui-react";
import Metrics from "./Metrics.js";
import RadialChart from "./RadialChart.js";
import LineChart from "./LineChart.js";
import Hfigure from "./Hfigure.js";
import RiskTrend from "./RiskTrend.js";
import { map_data } from "../helper.js";
import $ from "jquery";
class App extends Component {
	state = {
		c_observables: {},
		observables: {},
		risks: {},
		hgraph: {},
		factor: {},
		visible: true,
		radial_width: 600,
		chart_width: 600
	};
	componentDidMount() {
		$.ajax({
			url: "http://134.190.182.32:8080/api/citizen-elements/citizenAll/11",
			method: "GET",
			async: true,
			success: function(data) {
				this.setState(map_data(data));
				const disease_factor = this.state.observables;
				this.setState({
					hgraph: disease_factor,
					hgraph_title: "Overall Risk Factors"
				});
			}.bind(this)
		});
	}
	setChartWidth = () => {
		const width = document.getElementById("RadialChart").clientWidth;
		this.setState({ chart_width: width });
	};
	/*componentDidMount() {
		const data = { ...this.state.data };
		console.log(data);
	}
	transform = () => {
		const tdat = { ...this.state.data };
		console.log(tdat);
	};*/

	toggleMenu = () => this.setState({ visible: !this.state.visible });

	selectObs = () => {
		const Obs = this.state.observables;
		this.setState({ hgraph: Obs, hgraph_title: "Overall Risk Factors" });
	};
	selectRisk = (risk, disease) => {
		this.setState({ risk: risk, hgraph_title: disease });
		const disease_select = disease.replace(/\s+/gi, "");
		const disease_factor = this.state[disease_select];
		this.setState({ hgraph: disease_factor });
		//console.log(JSON.stringify(risk));
	};
	selectFactor = factor => {
		this.setState({ factor: factor });
	};
	setChartWidth = (e, { width }) => this.setState({ chart_width: width });
	setWidth = (e, { width }) => this.setState({ radial_width: width });

	render() {
		const {
			width,
			visible,
			risks,
			observables,
			factor,
			risk,
			hgraph,
			last_track,
			has,
			citizen_name,
			hgraph_title,
			radial_width,
			chart_width,
			age,
			gender
		} = this.state;

		const first_name = citizen_name;
		const risknumber = risks.length;
		var days = Math.floor(
			Math.abs(last_track - new Date().getTime()) / 1000 / 86400
		);
		const obs = Object.keys(observables).reduce(function(sum, key) {
			return sum + observables[key].measurements.length;
		}, 0);

		return (
			<div>
				<Menu inverted color="blue" size="huge">
					<Menu.Item onClick={this.toggleMenu} header fitted="vertically">
						<img src="/logo.png" />
						PRISM
					</Menu.Item>
					<Menu.Item name="HealthProfile" href="/healthprofile">
						<Icon name="doctor" />
						Hi, {first_name}
					</Menu.Item>
					<Menu.Item name="TrackChanges" href="/healthprofile">
						Age: {age}, Sex: {gender}
					</Menu.Item>
					<Menu.Menu position="right">
						<Menu.Item name="Account" href="/account">
							<Icon name="user" />
							Account
						</Menu.Item>
						<Menu.Item name="Logout" href="/">
							<Icon name="power" />
							Log Out
						</Menu.Item>
					</Menu.Menu>
				</Menu>
				<Sidebar.Pushable>
					<Sidebar
						as={Menu}
						animation="push"
						width="thin"
						visible={visible}
						icon="labeled"
						vertical
						inverted
						color="blue"
					>
						<Menu.Item name="dashboard" href="/home">
							<Icon name="home" />
							Dashboard
						</Menu.Item>
						<Menu.Item name="RiskSimulation" href="/risksimulation">
							<Icon name="chart line" />
							Risk Trends
						</Menu.Item>
						<Menu.Item name="Track" href="/track">
							<Icon name="edit" />
							Risk Comparison
						</Menu.Item>
						<Menu.Item name="HealthProfile" href="/healthprofile">
							<Icon name="heartbeat" />
							Health Profile
						</Menu.Item>
					</Sidebar>
					<Sidebar.Pusher>
						<Metrics days={days} obs={obs} has={has} risknumber={risknumber} />
						<Grid as={Segment}>
							<Grid.Row>
								<Grid.Column mobile={16} tablet={8} computer={6}>
									<RadialChart
										width={radial_width}
										data={risks}
										selectRisk={this.selectRisk}
										selectObs={this.selectObs}
									/>
								</Grid.Column>
								<Grid.Column mobile={16} tablet={12} computer={10}>
									<Hfigure
										width={radial_width}
										data={hgraph}
										title={hgraph_title}
										selectFactor={this.selectFactor}
									/>
								</Grid.Column>
							</Grid.Row>
							<Grid.Row>
								<Grid.Column mobile={16} tablet={8} computer={7}>
									<Responsive>
										<LineChart
											data={factor}
											title={"random"}
											id={"linechart"}
										/>
									</Responsive>
								</Grid.Column>
								<Grid.Column mobile={16} tablet={12} computer={9}>
									<Responsive>
										<RiskTrend data={risk} />
									</Responsive>
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</Sidebar.Pusher>
				</Sidebar.Pushable>
			</div>
		);
	}
}

export default App;
