import React from "react";
import d3 from "d3";
import { linegraph } from "../graphs/LineGraph.js";
import "../css/LineChart.css";
import { Card } from "semantic-ui-react";

class LineChart extends React.Component {
  componentDidUpdate() {
    this.removePreviousChart();
    this.draw();
  }
  removePreviousChart() {
    const chart = document.getElementById(this.props.id);
    while (chart.hasChildNodes()) chart.removeChild(chart.lastChild);
  }
  draw() {
    if (this.props.data.data) {
      var chart = linegraph()
        .width(document.getElementById(this.props.id).clientWidth)
        .height(this.props.height)
        .top_guideline(this.props.data.data.red_max)
        .bottom_guideline(this.props.data.data.red_min)
        .side_text_top(this.props.data.data.side_text_top)
        .side_text_mid(this.props.data.data.side_text_mid)
        .side_text_bot(this.props.data.data.side_text_bot)
        .red_max(this.props.data.data.red_max)
        .red_min(this.props.data.data.red_min)
        .yellow_max(this.props.data.data.yellow_max)
        .yellow_min(this.props.data.data.yellow_min)
        .title(this.props.data.data.label)
        .subtitle(this.props.data.data.units)
        .ylabel(this.props.data.data.units)
        .gradient_id(this.props.id + "_gradient");

      d3
        .select("#" + this.props.id)
        .datum(this.props.data.data.samples)
        .call(chart);
    }
  }

  render() {
    return (
      <Card color="blue" fluid>
        <Card.Content>
          <Card.Header as="h3">Risk Factor History</Card.Header>
        </Card.Content>
        <Card.Content>
          <div id={this.props.id} />
        </Card.Content>
      </Card>
    );
  }
}

LineChart.defaultProps = {
  width: 600,
  height: 350,

  // guideline Postions
  top_guideline: -50,
  bottom_guideline: -50,

  // Side legend variables
  side_text_top: "High",
  side_text_mid: "Normal",
  side_text_bot: "Low",

  //gradient changes
  red_max: 130,
  red_min: 40,
  yellow_max: 100,
  yellow_min: 50,

  //Title and Subtitle
  title: null,
  subtitle: null, // Subtitle for Chart => String

  //ylabel unit
  ylabel: "" // This is the unit of measurement for Y-axis => String
};
export default LineChart;
