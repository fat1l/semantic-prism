import React from "react";

const Notfound = () => (
	<div>
		<h2>Error, Page Not Found</h2>
	</div>
);

export default Notfound;
