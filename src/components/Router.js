import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./Login.js";
import Signup from "./Signup.js";
import App from "./App.js";
import Notfound from "./Notfound.js";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/home" component={App} />
      <Route path="/healthprofile" component={Signup} />
      <Route component={Notfound} />
    </Switch>
  </BrowserRouter>
);

export default Router;
