import React from 'react';
import ReactFauxDOM from 'react-faux-dom'
import d3 from 'd3'

class Polar extends React.Component{


draw_radialbar(container,data) {
            "use strict";
var div = container.replace('#','');
          var margin = 0,
            width = document.getElementById(div).clientWidth,
            height = document.getElementById(div).clientWidth,
            maxBarHeight = height / 2 - (margin + 70);

          var innerRadius = 0.3 * maxBarHeight; // innermost circle

          var svg = d3.select(container)
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("class", "chart")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

          var defs = svg.append("defs");

          var gradients = defs
            .append("linearGradient")
            .attr("id", "gradient-chart-area")
            //.attr("x1", "50%")
            //.attr("y1", "0%")
            //.attr("x2", "50%")
            //.attr("y2", "100%")
            .attr("spreadMethod", "pad");

          /*gradients.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", "#EDF0F0")
            .attr("stop-opacity", 1);*/

          gradients.append("stop")
            .attr("offset", "100%")
            .attr("stop-color", "#e3e3e3")
            .attr("stop-opacity", 1);

          gradients = defs
            .append("radialGradient")
            .attr("id", "gradient-bars")
            .attr("gradientUnits", "userSpaceOnUse")
            .attr("cx", "0")
            .attr("cy", "0")
            .attr("r", maxBarHeight)
            .attr("spreadMethod", "pad");

          gradients.append("stop")
            .attr("offset", "44%")
            .attr("stop-color", "#1FB25A");

          gradients.append("stop")
            .attr("offset", "40%")
            .attr("stop-color", "#FDB813");

          gradients.append("stop")
            .attr("offset", "86%")
            .attr("stop-color", "#F47032");

          gradients.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", "#EF402F");

          svg.append("circle")
            .attr("r", maxBarHeight)
            .classed("chart-area-circle", true);

          svg.append("circle")
            .attr("r", innerRadius)
            .classed("center-circle", true);

            //data = JSON.parse(data)
            data.forEach(function(x){
              x['latest_risk'] = x.samples.forEach(function(y){
                return (parseFloat(y.value))
              })
            })

            var cats = data.map(function(d, i) {
              return d.label;
            });

            console.log(data)
            var catCounts = {};
            for (var i = 0; i < cats.length; i++) {
              var num = cats[i];
              catCounts[num] = catCounts[num] ? catCounts[num] + 1 : 1;
            }
            // remove dupes (not exactly the fastest)
            cats = cats.filter(function(v, i) {
              return cats.indexOf(v) == i;
            });
            var numCatBars = cats.length;

            var angle = 0,
              rotate = 0;

            data.forEach(function(d, i) {
              // bars start and end angles
              d.startAngle = angle;
              angle += (2 * Math.PI) / numCatBars / catCounts[d.label];
              d.endAngle = angle;

              // y axis minor lines (i.e. questions) rotation
              d.rotate = rotate;
              rotate += 360 / numCatBars / catCounts[d.label];
            });

            // category_label
            var arc_category_label = d3.svg.arc()
              .startAngle(function(d, i) {
                return (i * 2 * Math.PI) / numCatBars;
              })
              .endAngle(function(d, i) {
                return ((i + 1) * 2 * Math.PI) / numCatBars;
              })
              .innerRadius(maxBarHeight + 20)
              .outerRadius(maxBarHeight + 24);

            var category_text = svg.selectAll("path.category_label_arc")
              .data(cats)
              .enter().append("path")
              .classed("category-label-arc", true)
              .attr("id", function(d, i) {
                return "category_label_" + i;
              }) //Give each slice a unique ID
              .attr("fill", "none")
              .attr("d", arc_category_label);

            category_text.each(function(d, i) {
              //Search pattern for everything between the start and the first capital L
              var firstArcSection = /(^.+?)L/;

              //Grab everything up to the first Line statement
              var newArc = firstArcSection.exec(d3.select(this).attr("d"))[1];
              //Replace all the commas so that IE can handle it
              newArc = newArc.replace(/,/g, " ");

              //If the whole bar lies beyond a quarter of a circle (90 degrees or pi/2)
              // and less than 270 degrees or 3 * pi/2, flip the end and start position
              var startAngle = (i * 2 * Math.PI) / numCatBars,
                endAngle = ((i + 1) * 2 * Math.PI) / numCatBars;

              if (startAngle > Math.PI / 2 && startAngle < 3 * Math.PI / 2 && endAngle > Math.PI / 2 && endAngle < 3 * Math.PI / 2) {
                var startLoc = /M(.*?)A/, //Everything between the capital M and first capital A
                  middleLoc = /A(.*?)0 0 1/, //Everything between the capital A and 0 0 1
                  endLoc = /0 0 1 (.*?)$/; //Everything between the 0 0 1 and the end of the string (denoted by $)
                //Flip the direction of the arc by switching the start and end point (and sweep flag)
                var newStart = endLoc.exec(newArc)[1];
                var newEnd = startLoc.exec(newArc)[1];
                var middleSec = middleLoc.exec(newArc)[1];

                //Build up the new arc notation, set the sweep-flag to 0
                newArc = "M" + newStart + "A" + middleSec + "0 0 0 " + newEnd;
              } //if

              //Create a new invisible arc that the text can flow along
              /*                            svg.append("path")
               .attr("class", "hiddenDonutArcs")
               .attr("id", "category_label_"+i)
               .attr("d", newArc)
               .style("fill", "none");*/

              // modifying existing arc instead
              d3.select(this).attr("d", newArc);
            });

            svg.selectAll(".category-label-text")
              .data(cats)
              .enter().append("text")
              .attr("class", "category-label-text")
              //.attr("x", 0)   //Move the text from the start angle of the arc
              //Move the labels below the arcs for those slices with an end angle greater than 90 degrees
              .attr("dy", function(d, i) {
                var startAngle = (i * 2 * Math.PI) / numCatBars,
                  endAngle = ((i + 1) * 2 * Math.PI) / numCatBars;
                return (startAngle > Math.PI / 2 && startAngle < 3 * Math.PI / 2 && endAngle > Math.PI / 2 && endAngle < 3 * Math.PI / 2 ? -4 : 14);
              })
              .append("textPath")
              .attr("fill", "black")
              .attr("startOffset", "50%")
              .style("text-anchor", "middle")
              .attr("xlink:href", function(d, i) {
                return "#category_label_" + i;
              })
              .text(function(d) {
                return d;
              });




            /* bars */
            var arc = d3.svg.arc()
              .startAngle(function(d, i) {
                return d.startAngle;
              })
              .endAngle(function(d, i) {
                return d.endAngle;
              })
              .innerRadius(innerRadius);

            var bars = svg.selectAll("path.bar")
              .data(data)
              .enter().append("path")
              .attr("class",function(d,i) { return "bar" + i; })
              .style("fill", "url(#gradient-bars)")
              .each(function(d) {
                d.outerRadius = innerRadius;
              })
              .attr("d", arc)
              .on("mouseenter", function(d) {
                    var arcOver = d3.svg.arc()
                      .innerRadius(innerRadius*1.1).outerRadius(d.outerRadius*1.2);

                    d3.select(this)
                       .transition()
                       .duration(500)
                       .attr("d", arcOver);
                 })
                 .on("mouseout", function(d) {
                    var arcOver = d3.svg.arc()
                      .innerRadius(innerRadius).outerRadius(d.outerRadius);

                    d3.select(this)
                     .transition()
                     .duration(800)
                     .attr("d", arcOver);
                 });

            bars.transition().ease("elastic").duration(1000).delay(function(d, i) {
                return i * 100;
              })
              .attrTween("d", function(d, index) {
                var i = d3.interpolate(d.outerRadius, x_scale(+d.sample.value));
                return function(t) {
                  d.outerRadius = i(t);
                  return arc(d, index);
                };
              });

            var x_scale = d3.scale.linear()
              .domain([0, 100])
              .range([innerRadius, maxBarHeight]);


            var y_scale = d3.scale.linear()
              .domain([0, 100])
              .range([-innerRadius, -maxBarHeight]);

            svg.selectAll("circle.x.minor")
              .data(y_scale.ticks())
              .enter().append("circle")
              .classed("gridlines minor", true)
              .attr("r", function(d) {
                return x_scale(d);
              });

            // category lines
            svg.selectAll("line.y.major")
              .data(cats)
              .enter().append("line")
              .classed("gridlines major", true)
              .attr('stroke-width', 420)
              .attr("y1", -innerRadius)
              .attr("y2", -maxBarHeight - 70)
              .attr("transform", function(d, i) {
                return "rotate(" + (i * 360 / numCatBars) + ")";
              });

            svg.selectAll("centerlabel")
              .data("Score").enter()
          	  .append("text")
                .attr("text-anchor", "middle")
            		.attr('font-size', '3em')
            		.style("font-weight","bold")
            		.attr('dy', 35)
            		//.attr("transform", "rotate(90)" )
            	  //.text(50+d3.sum(data, function(d) { return d.sample.value; }))
            	  .text(function(d) { return ((d3.sum(data, function(d) { return d.sample.value; }) < 50) ? "Low":"High")})
            	  .style("fill", function(d) { return ((d3.sum(data, function(d) { return d.sample.value; }) < 50) ? "#1FB25A":"#EF402F") });

            svg.selectAll("centerlabel1")
              .data("Risk Level").enter()
          	  .append("text")
                .attr("text-anchor", "middle")
            		.attr('font-size', '2em')
            		.attr('dy', 0)
            	  .text("Risk Level");
        }

        // https://bl.ocks.org/mbostock/7555321
        function wrapTextOnArc(text, radius) {
          // note getComputedTextLength() doesn't work correctly for text on an arc,
          // hence, using a hidden text element for measuring text length.
          var temporaryText = d3.select('svg')
            .append("text")
            .attr("class", "temporary-text") // used to select later
            .style("font", "7px sans-serif")
            .style("opacity", 0); // hide element

          var getTextLength = function(string) {
            temporaryText.text(string);
            return temporaryText.node().getComputedTextLength();
          };

          text.each(function(d) {
            var text = d3.select(this),
              words = text.text().split(/[ \f\n\r\t\v]+/).reverse(), //Don't cut non-breaking space (\xA0), as well as the Unicode characters \u00A0 \u2028 \u2029)
              word,
              wordCount = words.length,
              line = [],
              textLength,
              lineHeight = 1.1, // ems
              x = 0,
              y = 0,
              dy = 0,
              tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em"),
              arcLength = ((d.endAngle - d.startAngle) / (2 * Math.PI)) * (2 * Math.PI * radius),
              paddedArcLength = arcLength - 16;

            while (word = words.pop()) {
              line.push(word);
              tspan.text(line.join(" "));
              textLength = getTextLength(tspan.text());
              tspan.attr("x", (arcLength - textLength) / 2);

              if (textLength > paddedArcLength && line.length > 1) {
                // remove last word
                line.pop();
                tspan.text(line.join(" "));
                textLength = getTextLength(tspan.text());
                tspan.attr("x", (arcLength - textLength) / 2);

                // start new line with last word
                line = [word];
                tspan = text.append("tspan").attr("dy", lineHeight + dy + "em").text(word);
                textLength = getTextLength(tspan.text());
                tspan.attr("x", (arcLength - textLength) / 2);
              }
            }
          });

          d3.selectAll("text.temporary-text").remove()
        }

}
