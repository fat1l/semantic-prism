import React from "react";
import d3 from "d3";
import { Card, Icon, Header, Popup } from "semantic-ui-react";

class RadialChart extends React.PureComponent {
  componentDidMount() {
    this.draw_radialbar();
  }
  componentDidUpdate(nextprops) {
    this.removePreviousChart();
    this.draw_radialbar();
  }
  removePreviousChart() {
    const chart = document.getElementById("RadialChart");
    while (chart.hasChildNodes()) chart.removeChild(chart.lastChild);
  }
  draw_radialbar() {
    let data = this.props.data;
    var that = this;
    let container = "#RadialChart";

    var div = container.replace("#", "");
    var margin = 0,
      clientwidth = document.getElementById(div).clientWidth, //this.props.width > 600 ? 600 : Math.round(this.props.width),
      breakpoint = 680;

    var width = clientwidth > breakpoint ? 650 : clientwidth,
      height = clientwidth > breakpoint ? 650 : clientwidth,
      maxBarHeight = height / 1.85 - (margin + 70);

    var labelsize = width > 600 ? "1.5em" : "1.3em";
    var innerRadius = 0.3 * maxBarHeight; // innermost circle

    if (!data[1]) {
      console.log("No Data");
    } else {
      var svg = d3
        .select(container)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("class", "chart")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

      var defs = svg.append("defs");
      // Gradient Creation and Ranges
      var gradients = defs
        .append("linearGradient")
        .attr("id", "gradient-chart-area")
        //.attr("x1", "50%")
        //.attr("y1", "0%")
        //.attr("x2", "50%")
        //.attr("y2", "100%")
        .attr("spreadMethod", "pad");

      /*gradients.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", "#EDF0F0")
            .attr("stop-opacity", 1);*/

      gradients
        .append("stop")
        .attr("offset", "100%")
        .attr("stop-color", "#e3e3e3")
        .attr("stop-opacity", 1);

      gradients = defs
        .append("radialGradient")
        .attr("id", "gradient-bars")
        .attr("gradientUnits", "userSpaceOnUse")
        .attr("cx", "0")
        .attr("cy", "0")
        .attr("r", maxBarHeight)
        .attr("spreadMethod", "pad");

      gradients
        .append("stop")
        .attr("offset", "50%")
        .attr("stop-color", "#1FB25A");

      gradients
        .append("stop")
        .attr("offset", "40%")
        .attr("stop-color", "#FDB813");

      gradients
        .append("stop")
        .attr("offset", "86%")
        .attr("stop-color", "#F47032");

      gradients
        .append("stop")
        .attr("offset", "0%")
        .attr("stop-color", "#EF402F");

      svg
        .append("circle")
        .attr("r", maxBarHeight)
        .classed("chart-area-circle", true)
        .style({
          stroke: "#fff",
          "stroke-width": "3px",
          opacity: 0.13,
          fill: "url(#gradient-bars)"
        });

      svg
        .append("circle")
        .attr("r", innerRadius)
        .classed("center-circle", true)
        .style("fill", "white");

      //data = JSON.parse(data)
      data.forEach(function(x) {
        x["latest_risk"] = x.samples.forEach(function(y) {
          return parseFloat(y.value);
        });
      });

      var cats = data.map(function(d, i) {
        return d.label;
      });

      var catCounts = {};
      for (var i = 0; i < cats.length; i++) {
        var num = cats[i];
        catCounts[num] = catCounts[num] ? catCounts[num] + 1 : 1;
      }
      // remove dupes (not exactly the fastest)
      cats = cats.filter(function(v, i) {
        return cats.indexOf(v) === i;
      });
      var numCatBars = cats.length;

      var angle = 0,
        rotate = 0;

      data.forEach(function(d, i) {
        // bars start and end angles
        d.startAngle = angle;
        angle += 2 * Math.PI / numCatBars / catCounts[d.label];
        d.endAngle = angle;

        // y axis minor lines (i.e. questions) rotation
        d.rotate = rotate;
        rotate += 360 / numCatBars / catCounts[d.label];
      });

      // category_label
      var arc_category_label = d3.svg
        .arc()
        .startAngle(function(d, i) {
          return i * 2 * Math.PI / numCatBars;
        })
        .endAngle(function(d, i) {
          return (i + 1) * 2 * Math.PI / numCatBars;
        })
        .innerRadius(maxBarHeight + 20)
        .outerRadius(maxBarHeight + 24);

      var category_text = svg
        .selectAll("path.category_label_arc")
        .data(cats)
        .enter()
        .append("path")
        .classed("category-label-arc", true)
        .attr("id", function(d, i) {
          return "category_label_" + i;
        }) //Give each slice a unique ID
        .attr("fill", "none")
        .attr("d", arc_category_label);

      category_text.each(function(d, i) {
        //Search pattern for everything between the start and the first capital L
        var firstArcSection = /(^.+?)L/;

        //Grab everything up to the first Line statement
        var newArc = firstArcSection.exec(d3.select(this).attr("d"))[1];
        //Replace all the commas so that IE can handle it
        newArc = newArc.replace(/,/g, " ");

        //If the whole bar lies beyond a quarter of a circle (90 degrees or pi/2)
        // and less than 270 degrees or 3 * pi/2, flip the end and start position
        var startAngle = i * 2 * Math.PI / numCatBars,
          endAngle = (i + 1) * 2 * Math.PI / numCatBars;

        if (
          startAngle > Math.PI / 2 &&
          startAngle < 3 * Math.PI / 2 &&
          endAngle > Math.PI / 2 &&
          endAngle < 3 * Math.PI / 2
        ) {
          var startLoc = /M(.*?)A/, //Everything between the capital M and first capital A
            middleLoc = /A(.*?)0 0 1/, //Everything between the capital A and 0 0 1
            endLoc = /0 0 1 (.*?)$/; //Everything between the 0 0 1 and the end of the string (denoted by $)
          //Flip the direction of the arc by switching the start and end point (and sweep flag)
          var newStart = endLoc.exec(newArc)[1];
          var newEnd = startLoc.exec(newArc)[1];
          var middleSec = middleLoc.exec(newArc)[1];

          //Build up the new arc notation, set the sweep-flag to 0
          newArc = "M" + newStart + "A" + middleSec + "0 0 0 " + newEnd;
        } //if

        //Create a new invisible arc that the text can flow along
        /*                            svg.append("path")
               .attr("class", "hiddenDonutArcs")
               .attr("id", "category_label_"+i)
               .attr("d", newArc)
               .style("fill", "none");*/

        // modifying existing arc instead
        d3.select(this).attr("d", newArc);
      });

      svg
        .selectAll(".category-label-text")
        .data(cats)
        .enter()
        .append("text")
        .attr("class", "category-label-text")
        //.attr("x", 0)   //Move the text from the start angle of the arc
        //Move the labels below the arcs for those slices with an end angle greater than 90 degrees
        .attr("dy", function(d, i) {
          var startAngle = i * 2 * Math.PI / numCatBars,
            endAngle = (i + 1) * 2 * Math.PI / numCatBars;
          return startAngle > Math.PI / 2 &&
            startAngle < 3 * Math.PI / 2 &&
            endAngle > Math.PI / 2 &&
            endAngle < 3 * Math.PI / 2
            ? -4
            : 14;
        })
        .append("textPath")
        .attr("fill", "black")
        .attr("startOffset", "50%")
        .style("text-anchor", "middle")
        .style("font-weight", "bold")
        .style("font-size", labelsize)
        .attr("xlink:href", function(d, i) {
          return "#category_label_" + i;
        })
        .text(function(d) {
          return d;
        });

      /* bars */
      var arc = d3.svg
        .arc()
        .startAngle(function(d, i) {
          return d.startAngle;
        })
        .endAngle(function(d, i) {
          return d.endAngle;
        })
        .innerRadius(innerRadius);

      var bars = svg
        .selectAll("path.bar")
        .data(data)
        .enter()
        .append("path")
        .attr("class", function(d, i) {
          return "bar" + i;
        })
        .style("fill", "url(#gradient-bars)")
        .each(function(d) {
          d.outerRadius = innerRadius;
        })
        .attr("d", arc)
        .on("click", function(d) {
          that.props.selectRisk(d, d.label);
          var arcOver = d3.svg
            .arc()
            .innerRadius(innerRadius * 1.1)
            .outerRadius(d.outerRadius * 1.2);

          d3
            .select(this)
            .transition()
            .duration(50)
            .attr("d", arcOver);
        })
        .on("mouseout", function(d) {
          var arcOver = d3.svg
            .arc()
            .innerRadius(innerRadius)
            .outerRadius(d.outerRadius);

          d3
            .select(this)
            .transition()
            .duration(800)
            .attr("d", arcOver);
        });

      bars
        .transition()
        .ease("elastic")
        .duration(1000)
        .delay(function(d, i) {
          return i * 100;
        })
        .attrTween("d", function(d, index) {
          var i = d3.interpolate(d.outerRadius, x_scale(+d.samples[0].value));
          return function(t) {
            d.outerRadius = i(t);
            return arc(d, index);
          };
        });

      var x_scale = d3.scale
        .linear()
        .domain([0, 100])
        .range([innerRadius, maxBarHeight]);

      var y_scale = d3.scale
        .linear()
        .domain([0, 100])
        .range([-innerRadius, -maxBarHeight]);

      svg
        .selectAll("circle.x.minor")
        .data(y_scale.ticks())
        .enter()
        .append("circle")
        .classed("gridlines minor", true)
        .style({ fill: "none", stroke: "#fff", "stroke-width": "4px" })
        .attr("r", function(d) {
          return x_scale(d);
        });

      // category lines
      svg
        .selectAll("line.y.major")
        .data(cats)
        .enter()
        .append("line")
        .classed("gridlines major", true)
        .style({ fill: "none", stroke: "#fff", "stroke-width": "4px" })
        .attr("stroke-width", 420)
        .attr("y1", -innerRadius)
        .attr("y2", -maxBarHeight - 70)
        .attr("transform", function(d, i) {
          return "rotate(" + i * 360 / numCatBars + ")";
        });

      svg
        .selectAll("centerlabel")
        .data("Score")
        .enter()
        .append("text")
        .attr("text-anchor", "middle")
        .attr("font-size", "2em")
        .style("font-weight", "bold")
        .attr("dy", 25)
        //.attr("transform", "rotate(90)" )
        //.text(50+d3.sum(data, function(d) { return d.samples[0].value; }))
        .text(function(d) {
          return d3.sum(data, function(d) {
            return d.samples[0].value;
          }) < 50
            ? "Low"
            : "High";
        })
        .style("fill", function(d) {
          return d3.sum(data, function(d) {
            return d.samples[0].value;
          }) < 50
            ? "#1FB25A"
            : "#EF402F";
        })
        .on("click", function(d) {
          that.props.selectObs();
        });

      svg
        .selectAll("centerlabel1")
        .data("observables")
        .enter()
        .append("text")
        .attr("text-anchor", "middle")
        .attr("font-size", labelsize)
        .attr("dy", -5)
        .text("Risk Level")
        .on("click", function(d) {
          that.props.selectObs();
        });
    }
  }
  render() {
    return (
      <Card color="blue" fluid>
        <Card.Content>
          <Card.Header as="h3">
            Risk Snapshot{" "}
            <Popup trigger={<Icon name="info circle" />} inverted>
              Your risk snapshot shows 3 levels of risk:<br /> 1. High risk:
              this means that you have several risk factors that greatly
              increases your chance (risk) of developing a chronic disease. If
              you fall into this category for any disease, you will want to take
              serious disease prevention measures to lower your risk. <br /> 2.
              Moderate risk: This means your risk is slightly higher than what
              is considered low. If you are at moderate risk, making positive
              lifestyle changes can help to reduce your risk. <br />
              3. Low risk: this level means that your risk of having a chronic
              disease is lower than most people your age and sex. However, this
              does not mean you will never develop a chronic disease.
            </Popup>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <div id="RadialChart" />
        </Card.Content>
        <Card.Content>
          <Card.Description>
            <Header>What does your risk mean?</Header> Simply put, it is the
            chance that you develop a chronic disease, such as heart disease,
            diabetes, hypertension or cancer. Your risk is only an estimate, it
            does not tell you with 100% certainty if you will get a chronic
            disease. However, your risk shows your current health status with
            regards to common major chronic diseases and cancers. Your risk
            snapshot sums up your health risks with the ‘Health Asset Score’. It
            tells you about your overall health status, compared to people of
            your age and sex, based on several important chronic disease risk
            factors.
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}

RadialChart.defaultProps = {
  width: 600,
  data: []
};

export default RadialChart;
