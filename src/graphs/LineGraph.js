import d3 from "d3";
export function linegraph() {
	//Chart Size options
	var margin = { top: 50, right: 50, bottom: 50, left: 50 };
	var width;
	var height;

	// guideline Postions
	var top_guideline = 50;
	var bottom_guideline = -50;

	// Side legend variables
	var side_text_top;
	var side_text_mid;
	var side_text_bot;

	//gradient changes
	var red_max =130;
	var red_min = 40;
	var yellow_min =50;
	var yellow_max =100;
	var min;
	var max;

	//Title and Subtitle
	var title;
	var subtitle;

	//ylabel unit
	var ylabel;

	//gradient id
	var gradient_id;

	function chart(selection) {
		///////////////////////////////////////////////////
		//////////////// Set the Scales ///////////////////
		///////////////////////////////////////////////////
		selection.each(function(data) {
			// Housekeeping functions to be used later below
			// Keep min/max in memory
			var max = d3.max(data, function(d) {
				return d.value;
			});
			var min = d3.min(data, function(d) {
				return d.value;
			});

			//separate gradients if used multiple
			var grad_url = "url" + "(" + "#" + gradient_id + ")";

			//Set the x and y scale ranges
			var xScale = d3.time
				.scale()
				.domain([
					d3.min(data, function(d) {
						return d.timestamp - 1.8;
					}),
					d3.max(data, function(d) {
						return d.timestamp + 1.8e7;
					})
				])
				.range([0, width]);

			var yScale = d3.scale
				.linear()
				.domain([0, max * 1.3])
				.range([height, 0])
				.nice();

			// Define axes
			var xAxis = d3.svg
				.axis()
				.scale(xScale)
				.orient("bottom");

			var yAxis = d3.svg
				.axis()
				.scale(yScale)
				.orient("left")
				.ticks(5);

			// Set zoom behaviour
			var zoom = d3.behavior
				.zoom()
				.x(xScale)
				//.y(yScale)
				.scaleExtent([1, 3])
				.on("zoom", zoomed);

			/*var tip = d3
				.tip()
				.attr("class", "d3-tip")
				.offset([-10, 0])
				.html(function(d) {
					return (
						"<span>" +
						d.value +
						" " +
						ylabel +
						"</span><br><span>" +
						formatTime(d.date) +
						"</span>"
					);
				});*/

			///////////////////////////////////////////////////
			////////////// Initialize the SVG /////////////////
			///////////////////////////////////////////////////

			//Add the svg canvas for the line chart
			var svg = d3
				.select(this)
				.append("svg")
				.call(zoom)
				.attr("width", "100%")
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			//Add the X Axis
			svg
				.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);
			//Add the Y Axis
			svg
				.append("g")
				.attr("class", "y axis")
				.call(yAxis);

			//svg.call(tip);

			var titleWrapper = svg.append("g").attr("class", "titleWrapper");

			//Append title to the top
			titleWrapper
				.append("text")
				.attr("class", "title")
				.attr("x", width / 2)
				.attr("y", 10)
				.style("text-anchor", "middle")
				.text(title);
			titleWrapper
				.append("text")
				.attr("class", "subtitle")
				.attr("x", width / 2)
				.attr("y", 40)
				.style("text-anchor", "middle")
				.text(subtitle);

			var legendWrapper = svg.append("g").attr("class", "legendWrapper");

			//Legend
			legendWrapper
				.append("text")
				.attr("class", "axisLegend1")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(top_guideline * 1.1))
				.style("fill", "#D62C2B")
				.style("font-size", "1rem")
				.text(side_text_top);
			legendWrapper
				.append("text")
				.attr("class", "axisLegend2")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(bottom_guideline / 2.5))
				.style("fill", "#D62C2B")
				.style("font-size", "1rem")
				.text(side_text_bot);
			legendWrapper
				.append("text")
				.attr("class", "axisLegend3")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(bottom_guideline * 1.2))
				.style("fill", "#9C9C9C")
				.style("font-size", "1rem")
				.text(side_text_mid);

			svg
				.append("clipPath")
				.attr("id", "clip")
				.append("rect")
				.attr("width", width - 20)
				.attr("height", height);

			///////////////////////////////////////////////////
			//// Create the gradients and guidelines //////////
			///////////////////////////////////////////////////
			// Gradients
			var defs = svg.append("defs");

			var linearGradient = defs
				.append("linearGradient")
				.attr("id", gradient_id)
				.attr("gradientUnits", "userSpaceOnUse")
				.attr("x1", 0)
				.attr("y1", 0)
				.attr("x2", 0)
				.attr("y2", height);

			linearGradient
				.append("stop")
				.attr("class", "top")
				.attr("offset", yScale(red_max) / yScale.range()[0])
				.attr("stop-color", "#D62C2B");

			linearGradient
				.append("stop")
				.attr("class", "middle")
				.attr("offset", yScale(yellow_max) / yScale.range()[0])
				.attr("stop-color", "orange");

			linearGradient
				.append("stop")
				.attr("class", "middle")
				.attr("offset", yScale(yellow_max) / yScale.range()[0])
				.attr("stop-color", "orange");

			linearGradient
				.append("stop")
				.attr("class", "middle")
				.attr("offset", yScale(yellow_max) / yScale.range()[0])
				.attr("stop-color", "#00FF00");
			linearGradient
				.append("stop")
				.attr("class", "middle")
				.attr("offset", yScale(yellow_min) / yScale.range()[0])
				.attr("stop-color", "#00FF00");

			linearGradient
				.append("stop")
				.attr("class", "middle")
				.attr("offset", yScale(yellow_min) / yScale.range()[0])
				.attr("stop-color", "orange");

		linearGradient
			.append("stop")
			.attr("class", "middle")
			.attr("offset", yScale(yellow_min) / yScale.range()[0])
			.attr("stop-color", "orange");

			linearGradient
				.append("stop")
				.attr("class", "bottom")
				.attr("offset", yScale(red_min) / yScale.range()[0])
				.attr("stop-color", "#D62C2B");

			// Guidelines for Ranges
			svg
				.append("line")
				.attr("class", "guide2")
				.style("stroke", "grey")
				.attr("stroke-dasharray", 4)
				.style("stroke-width", "1px")
				//.attr('x1', margin.left)
				.attr("y1", yScale(top_guideline))
				.attr("x2", width)
				.attr("y2", yScale(top_guideline))
				.attr("clip-path", "url(#clip)");

			svg
				.append("line")
				.attr("class", "guide1")
				.style("stroke", "grey")
				.attr("stroke-dasharray", 4)
				.style("stroke-width", "1px")
				//.attr('x1', margin.left)
				.attr("y1", yScale(bottom_guideline))
				.attr("x2", width)
				.attr("y2", yScale(bottom_guideline))
				.attr("clip-path", "url(#clip)");

			///////////////////////////////////////////////////
			/////////////// Draw the actual Data //////////////
			///////////////////////////////////////////////////

			//Initiate the line function
			var lineFunction = d3.svg
				.line()
				.interpolate("monotone")
				.x(function(d) {
					return xScale(d.timestamp);
				})
				.y(function(d) {
					return yScale(d.value);
				});

			// Draw the data line
			svg
				.selectAll(".line")
				.data(data)
				.enter()
				.append("path")
				.attr("class", "line")
				.attr("clip-path", "url(#clip)")
				.style("stroke", grad_url)
				.attr("d", lineFunction(data));

			// Draw the scatterpoints
			svg
				.selectAll(".lineDots")
				.data(data, function(d) {
					return d.timestamp;
				})
				.enter()
				.append("circle")
				.attr("class", "lineDots")
				.attr("r", 4)
				.style("stroke", grad_url)
				.attr("cx", function(d) {
					return xScale(d.timestamp);
				})
				.attr("cy", function(d) {
					return yScale(d.value);
				})
				.attr("clip-path", "url(#clip)");
			//.on("mouseover", tip.show)
			//.on("mouseout", tip.hide);

			//Label the highest and lowest points
			svg
				.selectAll(".marker_label")
				.data(data)
				.enter()
				.append("text")
				.attr("class", "marker_label")
				.attr("x", function(d) {
					return xScale(d.timestamp);
				})
				.attr("y", function(d) {
					return yScale(d.value);
				})
				.attr("dx", "-1.5em")
				.attr("dy", "2em")
				.attr("clip-path", "url(#clip)")
				.text(function(d) {
					return d.value >= max
						? d.value + " " + ylabel
						: d.value <= min ? d.value + " " + ylabel : "";
				});

			function zoomed() {
				svg.select(".x.axis").call(xAxis);
				svg.selectAll("path.line").attr("d", lineFunction(data));
				svg
					.selectAll(".lineDots")
					.data(data, function(d) {
						return d.timestamp;
					})
					.attr("cx", function(d) {
						return xScale(d.timestamp);
					})
					.attr("cy", function(d) {
						return yScale(d.value);
					});
				svg
					.selectAll(".marker_label")
					.data(data, function(d) {
						return d.timestamp;
					})
					.attr("x", function(d) {
						return xScale(d.timestamp);
					});
			}
		});
	}

	chart.width = function(value) {
		if (!arguments.length) return width;
		width = value - 55;
		return chart;
	};

	chart.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return chart;
	};

	chart.red_max = function(value) {
		if (!arguments.length) return red_max;
		red_max = value;
		return chart;
	};

	chart.red_min = function(value) {
		if (!arguments.length) return red_min;
		red_min = value;
		return chart;
	};

	chart.yellow_min = function(value) {
		if (!arguments.length) return yellow_min;
		yellow_min = value;
		return chart;
	};
	chart.yellow_max = function(value) {
		if (!arguments.length) return yellow_max;
		yellow_max = value;
		return chart;
	};
	chart.min = function(value) {
		if (!arguments.length) return min;
		min = value;
		return chart;
	};
	chart.max = function(value) {
		if (!arguments.length) return max;
		max = value;
		return chart;
	};

	chart.top_guideline = function(value) {
		if (!arguments.length) return top_guideline;
		top_guideline = value;
		red_max = value;
		return chart;
	};

	chart.bottom_guideline = function(value) {
		if (!arguments.length) return bottom_guideline;
		bottom_guideline = value;
		red_min = value;
		return chart;
	};

	chart.side_text_top = function(value) {
		if (!arguments.length) return side_text_top;
		side_text_top = value;
		return chart;
	};

	chart.side_text_mid = function(value) {
		if (!arguments.length) return side_text_mid;
		side_text_mid = value;
		return chart;
	};

	chart.side_text_bot = function(value) {
		if (!arguments.length) return side_text_bot;
		side_text_bot = value;
		return chart;
	};

	chart.ylabel = function(value) {
		if (!arguments.length) return ylabel;
		ylabel = value;
		return chart;
	};

	chart.title = function(value) {
		if (!arguments.length) return title;
		title = value;
		return chart;
	};

	chart.subtitle = function(value) {
		if (!arguments.length) return subtitle;
		subtitle = value;
		return chart;
	};

	chart.gradient_id = function(value) {
		if (!arguments.length) return gradient_id;
		gradient_id = value;
		return chart;
	};

	return chart;
}
