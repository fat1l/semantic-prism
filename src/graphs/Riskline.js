import d3 from "d3";
export function risk_line() {
	//Chart Size options
	var margin = { top: 50, right: 0, bottom: 50, left: 30 };
	var width;
	var height;

	// guideline Postions
	var top_guideline;
	var bottom_guideline;

	// Side legend variables
	var side_text_top;
	var side_text_mid;
	var side_text_bot;

	//gradient changes
	var red_max;
	var red_min;
	var yellow_min;
	var yellow_max;
	var min;
	var max;

	//Title and Subtitle
	var title;
	var subtitle;

	//ylabel unit
	var ylabel;

	//gradient id
	var gradient_id;

	function chart(selection) {
		///////////////////////////////////////////////////
		//////////////// Set the Scales ///////////////////
		///////////////////////////////////////////////////
		selection.each(function(data) {
			// Housekeeping functions to be used later below
			// Keep min/max in memory
			var max_value = d3.max(data, function(d) {
				return d.value;
			});
			var min_value = d3.min(data, function(d) {
				return d.value;
			});

			//Set the x and y scale ranges
			var xScale = d3.time
				.scale()
				.domain([
					d3.min(data, function(d) {
						return d.timestamp;
					}),
					d3.max(data, function(d) {
						return d.timestamp;
					})
				])
				.range([0, width]);

			var yScale = d3.scale
				.linear()
				.domain([
					d3.min(data, function(d) {
						return d.value - 5;
					}),
					d3.max(data, function(d) {
						return d.value + 15;
					})
				])
				.range([height, 0])
				.nice();

			// Define axes
			var xAxis = d3.svg
				.axis()
				.scale(xScale)
				.orient("bottom");

			var yAxis = d3.svg
				.axis()
				.scale(yScale)
				.orient("left")
				.ticks(5);

			// Set zoom behaviour
			var zoom = d3.behavior
				.zoom()
				.x(xScale)
				//.y(yScale)
				.scaleExtent([1, 3])
				.on("zoom", zoomed);

			///////////////////////////////////////////////////
			////////////// Initialize the SVG /////////////////
			///////////////////////////////////////////////////

			//Add the svg canvas for the line chart
			var svg = d3
				.select(this)
				.append("svg")
				.call(zoom)
				.attr("width", "100%")
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			//Add the X Axis
			svg
				.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);
			//Add the Y Axis
			svg
				.append("g")
				.attr("class", "y axis")
				.call(yAxis);

			//svg.call(tip);

			var titleWrapper = svg.append("g").attr("class", "titleWrapper");

			//Append title to the top
			titleWrapper
				.append("text")
				.attr("class", "title")
				.attr("x", width / 2)
				.attr("y", 10)
				.style("text-anchor", "middle")
				.text(title);
			titleWrapper
				.append("text")
				.attr("class", "subtitle")
				.attr("x", width / 2)
				.attr("y", 40)
				.style("text-anchor", "middle")
				.text(subtitle);

			var legendWrapper = svg.append("g").attr("class", "legendWrapper");

			//Legend
			legendWrapper
				.append("text")
				.attr("class", "axisLegend1")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(top_guideline * 1.1))
				.style("fill", "#D62C2B")
				.style("font-size", "1rem")
				.text(side_text_top);
			legendWrapper
				.append("text")
				.attr("class", "axisLegend2")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(bottom_guideline / 2.5))
				.style("fill", "green")
				.style("font-size", "1rem")
				.text(side_text_bot);
			legendWrapper
				.append("text")
				.attr("class", "axisLegend3")
				.attr("transform", "rotate(-90)")
				.attr("y", width)
				.attr("x", -yScale(bottom_guideline * 1.3))
				.style("fill", "orange")
				.style("font-size", "1rem")
				.text(side_text_mid);

			svg
				.append("clipPath")
				.attr("id", "clip1")
				.append("rect")
				.attr("width", width > 900 ? width - 50 : width - 20)
				.attr("height", height);

			///////////////////////////////////////////////////
			//// Create the guidelines //////////
			///////////////////////////////////////////////////
			// Gradients
			//separate gradients if used multiple
			var grad_url = "url" + "(" + "#" + gradient_id + ")";
			var defs = svg.append("defs");

			var linearGradient = defs
				.append("linearGradient")
				.attr("id", gradient_id)
				.attr("gradientUnits", "userSpaceOnUse")
				.attr("x1", 0)
				.attr("y1", 0)
				.attr("x2", 0)
				.attr("y2", height);

			linearGradient
				.append("stop")
				.attr("class", "top")
				.attr("offset", yScale(min) / yScale.range()[0])
				.attr("stop-color", "green");

			linearGradient
				.append("stop")
				.attr("class", "top")
				.attr("offset", yScale(yellow_max) / yScale.range()[0])
				.attr("stop-color", "orange");
			linearGradient
				.append("stop")
				.attr("class", "bottom")
				.attr("offset", yScale(red_max) / yScale.range()[0])
				.attr("stop-color", "orange");

			linearGradient
				.append("stop")
				.attr("class", "bottom")
				.attr("offset", yScale(red_max) / yScale.range()[0])
				.attr("stop-color", "#D62C2B");
			// Guidelines for Ranges
			svg
				.append("line")
				.attr("class", "guide2")
				.style("stroke", "grey")
				.attr("stroke-dasharray", 4)
				.style("stroke-width", "1px")
				//.attr('x1', margin.left)
				.attr("y1", yScale(top_guideline))
				.attr("x2", width)
				.attr("y2", yScale(top_guideline))
				.attr("clip-path", "url(#clip1)");

			svg
				.append("line")
				.attr("class", "guide1")
				.style("stroke", "grey")
				.attr("stroke-dasharray", 4)
				.style("stroke-width", "1px")
				//.attr('x1', margin.left)
				.attr("y1", yScale(bottom_guideline))
				.attr("x2", width)
				.attr("y2", yScale(bottom_guideline))
				.attr("clip-path", "url(#clip1)");

			///////////////////////////////////////////////////
			/////////////// Draw the actual Data //////////////
			///////////////////////////////////////////////////

			//Initiate the line function
			var lineFunction = d3.svg
				.line()
				.interpolate("monotone")
				.x(function(d) {
					return xScale(d.timestamp);
				})
				.y(function(d) {
					return yScale(d.value);
				});

			//Initiate the area line function
			var areaFunction = d3.svg
				.area()
				.interpolate("monotone")
				.x(function(d) {
					return xScale(d.timestamp);
				})
				.y0(function(d) {
					// the lowest point
					if (d[1] <= 0) {
						return yScale(height);
					} else {
						return yScale(0);
					}
				})
				.y1(function(d) {
					return yScale(d.value);
				});

			//Draw the underlying area chart filled with the gradient
			/*svg.selectAll('.area')
				.data(data)
				.enter()
				.append("path")
			    .attr("class", "area")
				.attr("clip-path", "url(#clip)")
				.style("fill", grad_url)
				//.attr("stroke", "grey")
			    .attr("d", areaFunction(data));*/

			// Draw the data line
			svg
				.selectAll(".line")
				.data(data)
				.enter()
				.append("path")
				.attr("class", "line")
				.attr("clip-path", "url(#clip1)")
				.attr("stroke", "#A9A9A9")
				.attr("d", lineFunction(data));

			// Draw the scatterpoints
			svg
				.selectAll(".lineDots")
				.data(data, function(d) {
					return d.timestamp;
				})
				.enter()
				.append("circle")
				.attr("class", "lineDots")
				.attr("r", function(d) {
					return d.value == max_value ? 10 : 5;
				})
				.style("fill", function(d) {
					return (d.value > red_max)
						? "#D62C2B"
						: (d.value > yellow_max)
							? "orange"
							: d.value === min ? "white" : "#00FF00";
				})
				.style("stroke", "grey")
				.attr("cx", function(d) {
					return xScale(d.timestamp);
				})
				.attr("cy", function(d) {
					return yScale(d.value);
				})
				.attr("clip-path", "url(#clip1)");
			//.on("mouseover", tip.show)
			//.on("mouseout", tip.hide);

			//Label the highest and lowest points
			/*svg
				.selectAll(".marker_label")
				.data(data)
				.enter()
				.append("text")
				.attr("class", "marker_label")
				.attr("x", function(d) {
					return xScale(d.timestamp);
				})
				.attr("y", function(d) {
					return yScale(d.value);
				})
				.attr("dx", "-0.5em")
				.attr("dy", "2em")
				.attr("clip-path", "url(#clip1)")
				.text(function(d) {
					return (d.value === min_value) | (d.value === max_value)
						? d.value + " " + ylabel
						: ""
				});*/

			function zoomed() {
				svg.select(".x.axis").call(xAxis);
				svg.selectAll("path.line").attr("d", lineFunction(data));
				svg
					.selectAll(".lineDots")
					.data(data, function(d) {
						return d.timestamp;
					})
					.attr("cx", function(d) {
						return xScale(d.timestamp);
					})
					.attr("cy", function(d) {
						return yScale(d.value);
					});
				svg
					.selectAll(".marker_label")
					.data(data, function(d) {
						return d.timestamp;
					})
					.attr("x", function(d) {
						return xScale(d.timestamp);
					});
			}
		});
	}

	chart.width = function(value) {
		if (!arguments.length) return width;
		width = value - 50;
		return chart;
	};

	chart.height = function(value) {
		if (!arguments.length) return height;
		height = value;
		return chart;
	};

	chart.red_max = function(value) {
		if (!arguments.length) return red_max;
		red_max = value;
		return chart;
	};

	chart.red_min = function(value) {
		if (!arguments.length) return red_min;
		red_min = value;
		return chart;
	};
	chart.yellow_min = function(value) {
		if (!arguments.length) return yellow_min;
		yellow_min = value;
		return chart;
	};
	chart.yellow_max = function(value) {
		if (!arguments.length) return yellow_max;
		yellow_max = value;
		return chart;
	};
	chart.min = function(value) {
		if (!arguments.length) return min;
		min = value;
		return chart;
	};
	chart.max = function(value) {
		if (!arguments.length) return max;
		max = value;
		return chart;
	};

	chart.top_guideline = function(value) {
		if (!arguments.length) return top_guideline;
		top_guideline = value;
		return chart;
	};

	chart.bottom_guideline = function(value) {
		if (!arguments.length) return bottom_guideline;
		bottom_guideline = value;
		return chart;
	};

	chart.side_text_top = function(value) {
		if (!arguments.length) return side_text_top;
		side_text_top = value;
		return chart;
	};

	chart.side_text_mid = function(value) {
		if (!arguments.length) return side_text_mid;
		side_text_mid = value;
		return chart;
	};

	chart.side_text_bot = function(value) {
		if (!arguments.length) return side_text_bot;
		side_text_bot = value;
		return chart;
	};

	chart.ylabel = function(value) {
		if (!arguments.length) return ylabel;
		ylabel = value;
		return chart;
	};

	chart.title = function(value) {
		if (!arguments.length) return title;
		title = value;
		return chart;
	};

	chart.subtitle = function(value) {
		if (!arguments.length) return subtitle;
		subtitle = value;
		return chart;
	};
	chart.gradient_id = function(value) {
		if (!arguments.length) return gradient_id;
		gradient_id = value;
		return chart;
	};

	return chart;
}
