import $ from "jquery";
var Enumerable = require("linq");

export function map_data(data) {
	function grouper(propertyName, selector) {
		return function(e) {
			return e.groupBy("$." + propertyName, null, function(k, g) {
				return {
					...(g.id === 239 ? { label: "Vitals" } : { label: k }),
					measurements: g.letBind(selector).toArray()
				};
			});
		};
	}

	function grouper_samples(propertyName, selector) {
		return function(e) {
			return e.groupBy("$." + propertyName, null, function(k, g) {
				return {
					label: k
						.replace(/10|6|4|risk|-year|rescaled|or processed/gi, "")
						.trim(),
					...(k === "Current weight" ? { units: "Kg", min: 55, max: 100 } : {}),
					...(k === "Body mass index"
						? {
								units: "Kg/m",
								min: 19,
								max: 24,
								yellow_min: 18,
								yellow_max: 25,
								red_min: 16.5,
								red_max: 30,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Waist circumference"
						? {
							units: "inches",
							min: 25,
							max: 35,
							yellow_min: 24,
							yellow_max: 36,
							red_min: 20,
							red_max: 40,
							side_text_top: "Above Normal",
							side_text_mid: "Normal",
							side_text_bot: "Below Normal"
						  }
						: {}),
					...(k === "Systolic blood pressure"
						? {
								units: "mm/hg",
								min: 90,
								max: 129,
								yellow_min: 85,
								yellow_max: 130,
								red_min: 80,
								red_max: 140,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
						...(k === "Current weight"
							? {
									units: "kg",
									min: 50,
									max: 90/*,
									yellow_min: 49,
									yellow_max: 91,
									red_min: 39,
									red_max: 110,
									side_text_top:"Overweight",
									side_text_mid:"",
									side_text_bot:"Underweight"*/
							  }
							: {}),
					...(k === "Diastolic blood pressure"
						? {
								units: "mm/hg",
								min: 70,
								max: 90,
								yellow_min: 60,
								yellow_max: 91,
								red_min: 50,
								red_max: 100,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Total cholesterol"
						? {
								units: "mmol/l",
								min: 0,
								max: 5,
								yellow_min: 1,
								yellow_max: 6,
								red_min: 0,
								red_max: 7,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:""
						  }
						: {}),
					...(k === "HDL cholesterol"
						? {
								units: "mmol/l",
								min: 0,
								max: 3,
								yellow_min: 1.5,
								yellow_max: 4,
								red_max: 5,
								red_min: 1,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Blood glucose"
						? {
								units: "mmol/l",
								min: 2,
								max: 7,
								yellow_min: 3.9,
								yellow_max: 6.1,
								red_min: 3.5,
								red_max: 6.2,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Cigarette smoking"
						? {
								units: "Cigarettes/day",
								min: 0,
								max: 5,
								//yellow_min: -20,
								yellow_max: 1,
								red_min: -25,
								red_max: 10,
								side_text_top:"Above Normal",
								side_text_mid:"",
								side_text_bot:""
						  }
						: {}),
					...(k === "Physical activity"
						? {
								units: "Hours/week",
								min: 3,
								max: 40,
								yellow_min: 2,
								yellow_max: 41,
								red_max: 50,
								red_min: 0,
								side_text_top:"Great",
								side_text_mid:"Normal",
								side_text_bot:""
						  }
						: {}),
					...(k === "Daily fruit intake"
						? {
								units: "Serving(s)/day",
								min: 3,
								max: 14,
								yellow_min: 2,
								yellow_max: 15,
								red_min: 1,
								red_max: 20,
								side_text_top:"",
								side_text_mid:"",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Daily vegetable intake"
						? {
								units: "Serving(s)/day",
								min: 4,
								max: 50,
								yellow_min: 2,
								red_min: 0,
								red_max: 40,
								side_text_top:"",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Red or processed meat intake"
						? {
								units: "Serving(s)/week",
								min: 0,
								max: 2,
								yellow_max: 3,
								red_min: 0,
								red_max: 4,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Dairy products intake"
						? {
								units: "Serving(s)/day",
								min: 3,
								max: 50,
								yellow_min: 2,
								red_min: 0,
								red_max: 15,
								side_text_top:"",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					...(k === "Alcohol"
						? {
								units: "Drinks/week",
								min: 0,
								max: 7,
								yellow_max: 8,
								red_min: -1,
								red_max: 10,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:""
						  }
						: {}),
					...(k === "Weight gain since 18 years of age "
						? {
								units: "Kg",
								min: 0,
								max: 7,
								yellow_max: 8,
								red_min: -1,
								red_max: 10,
								side_text_top:"Above Normal",
								side_text_mid:"Normal",
								side_text_bot:"Below Normal"
						  }
						: {}),
					samples: g.letBind(selector).toArray()
				};
			});
		};
	}

	var query = Enumerable.from(data)
		//.Where("$.element.short_name =='O1'")
		.where(function(x) {
			return (
				x.element.category !== null &&
				(x.element.category.name !== "Body measurements" &&
					x.element.category.name !== "Vitals and lab tests" &&
					x.element.category.name !== "Lifestyle factors")
			);
		})
		.groupBy(
			function(x) {
				return x.element.category.name;
			}, // key
			function(x) {
				return { label: x.element.full_name, value: x.value };
			},
			"{ label: $, measurements: $$.toArray() }" // items
		)
		.toArray();

	var items = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.category !== null &&
				(x.element.id === 704 || // Physical Activity
				x.element.id === 701 || // Smoking
				x.element.id === 720 || // Alcohol
				x.element.id === 706 || // Fruit intake
				x.element.id === 707 || // Vegetable intake
				x.element.id === 713 || // Diary products intake
				x.element.id === 708 || // Red meat intake
				x.element.id === 653 || // SBP
				x.element.id === 654 || //DBP
				x.element.id === 657 || // Blood glucose
				x.element.id === 655 || // Totol cholesterol
				x.element.id === 656 || // HDL
				x.element.id === 645 || // BMI
				x.element.id === 646 || // Waist
				x.element.id === 643) // Weight
			);
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var risks = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.element_type.name === "ASSOCIATION" &&
				x.value !== "0.00" &&
				x.element.full_name !== "Age" &&
				x.element.full_name !== "Body mass index" &&
				x.element.full_name !== "Dietary risks" &&
				x.element.full_name.match(/-year/i) //getting only year risks => absolute risks
			);
		})
		.letBind(
			grouper_samples("element.full_name", function(g1) {
				return g1
					.select("{timestamp: $.date_entered, value: $.value}")
					.toArray();
			})
		)
		.toArray();

	var cv = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 653 || // SBP
				x.element.id === 654 || // DBP
				x.element.id === 655 || // Total Cholesterol
				x.element.id === 656 || //HDL Cholesterol
				x.element.id === 701
			); //Smoking
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var dm = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 646 || // Waist circumference
				x.element.id === 706 || // Fruit Intake
				x.element.id === 707 || // Vegetable Intake
				x.element.id === 657
			); // Blood glucose
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var ht = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 653 || // Systolic
				x.element.id === 654 || // Diastolic
				x.element.id === 645 || // BMI
				x.element.id === 701
			); // Smoking
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var colon_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 704 || // Physical activity
				x.element.id === 720 || // Alcohol
				x.element.id === 701 || // Smoking
				x.element.id === 713
			); // Diary Products
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var kidney_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 701 || // Smoking
				x.element.id === 653 || // Systolic
				x.element.id === 654
			); // Diastolic
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var pancreatic_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 706 || // Vegetable Intake
				x.element.id === 707 || // Fruit Intake
				x.element.id === 701 || // Smoking
				x.element.id === 720 || //Alcohol
				x.element.id === 704
			); //Physical Activity
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var lung_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 701 || // Smoking
				x.element.id === 702
			); //Duration of Smoking
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var breast_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 647 || // Adult weight gain
				x.element.id === 704 || // Physical activity
				x.element.id === 720
			); //Alcohol
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var uterine_cancer = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.id === 645 || // BMI
				x.element.id === 701 || // Smoking
				x.element.id === 704
			); // Physical Activity
		})
		.letBind(
			grouper("element.category.name", function(g1) {
				return g1.letBind(
					grouper_samples("element.full_name", function(g2) {
						return g2
							.select(function(x) {
								return {
									timestamp: x.date_entered,
									value: parseFloat(x.value)
								};
							})
							.toArray();
					})
				);
			})
		)
		.toArray();

	var flat_risks = Enumerable.from(data)
		.where(function(x) {
			return (
				x.element.element_type.name === "ASSOCIATION" &&
				x.value !== "0.00" &&
				x.element.full_name !== "Age" &&
				x.element.full_name !== "Body mass index" &&
				x.element.full_name !== "Dietary risks" &&
				x.element.full_name.match(/-year/i) //getting only year risks => absolute risks
			);
		})
		.select(function(x) {
			return {
				key: x.element.full_name
					.replace(/10|6|4|risk|-year|rescaled/gi, "")
					.trim(),
				timestamp: x.date_entered,
				value: parseFloat(x.value)
			};
		})
		.toArray();
	var last_track = Enumerable.from(data)
		.select(function(x) {
			return x.date_entered;
		})
		.maxBy(function(x) {
			return x.date_entered;
		});

	var has = Enumerable.from(data)
		.where(function(x) {
			return x.element.short_name === "A22";
		})
		.select(function(x) {
			return x.value;
		})
		.maxBy(function(x) {
			return x.date_entered;
		});

	var risk = Enumerable.from(data)
		.where(function(x) {
			return x.element.short_name === "A22";
		})
		.letBind(
			grouper_samples("element.full_name", function(g1) {
				return g1
					.select("{timestamp: $.date_entered, value: $.value}")
					.toArray();
			})
		)
		.toArray();

		var citizen_name = Enumerable.from(data)
											.select(function(x){return x.citizen.user.firstName})
											.first();

		var age = Enumerable.from(data)
											.where(function(x){return x.element.id === 638})
											.select(function(x){return x.value})
											.first();

		var gender = Enumerable.from(data)
											.where(function(x){return x.element.id === 639})
											.select(function(x){return x.value})
											.first();
	const final = {
		citizen_name: citizen_name,
		age:age,
		gender:gender,
		last_track: last_track,
		has: has,
		observables: items,
		risks: risks,
		risk: risk[0],
		c_observables: query,
		steamgraph: flat_risks,
		Cardiovascular: cv,
		Diabetes: dm,
		Hypertension: ht,
		Coloncancer: colon_cancer,
		Kidneycancer: kidney_cancer,
		Pancreaticcancer: pancreatic_cancer,
		Lungcancer: lung_cancer,
		Breastcancer: breast_cancer,
		Uterinecancer: uterine_cancer
	};
	return final;
}
